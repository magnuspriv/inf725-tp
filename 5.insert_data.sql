-- INSERT DATA BLOCK

BEGIN TRANSACTION;

-- manage fk constraint
INSERT INTO rayon VALUES('jouet',1,null);
INSERT INTO rayon VALUES('vetement',2, null);
INSERT INTO rayon VALUES('jardin',3, null);

INSERT INTO employe VALUES('durand',1000,1);
INSERT INTO employe VALUES('dubois',1500,1);
INSERT INTO employe VALUES('dupont',2000,1);
INSERT INTO employe VALUES('dumoulin',1200,2);
INSERT INTO employe VALUES('dutilleul',1000,2);
INSERT INTO employe VALUES('duchene',2000,2);
INSERT INTO employe VALUES('duguesclin',1500,3);
INSERT INTO employe VALUES('duduche',2000,3);

-- manage fk constraint
update  rayon set chef_rayon = 'dupont' where no_rayon=1;
update  rayon set chef_rayon = 'duchene' where no_rayon=2;
update  rayon set chef_rayon = 'duduche' where no_rayon=3;


INSERT INTO produit VALUES('train',1,1,100);
INSERT INTO produit VALUES('avion',2,1,75);
INSERT INTO produit VALUES('bateau',3,1,70);
INSERT INTO produit VALUES('pantalon',4,2,30);
INSERT INTO produit VALUES('veste',5,2,38);
INSERT INTO produit VALUES('robe',6,2,50);
INSERT INTO produit VALUES('rateau',7,3,5);
INSERT INTO produit VALUES('pioche',8,3,7);
INSERT INTO produit VALUES('brouette',9,3,38);

INSERT INTO fournisseur VALUES('f1','paris');
INSERT INTO fournisseur VALUES('f2','lyon');
INSERT INTO fournisseur VALUES('f3','marseille');

INSERT INTO fournir VALUES('f1',1,90);
INSERT INTO fournir VALUES('f2',2,70);
INSERT INTO fournir VALUES('f2',3,60);
INSERT INTO fournir VALUES('f1',4,25);
INSERT INTO fournir VALUES('f1',5,30);
INSERT INTO fournir VALUES('f2',6,45);
INSERT INTO fournir VALUES('f1',7,4);
INSERT INTO fournir VALUES('f3',8,5);
INSERT INTO fournir VALUES('f3',9,32);

INSERT INTO client VALUES('dumont','paris',500);
INSERT INTO client VALUES('dupont','lyon',-200);
INSERT INTO client VALUES('dupond','marseille',-300);
INSERT INTO client VALUES('dulac','paris',800);
INSERT INTO client VALUES('dumas','lyon',-300);

INSERT INTO commande VALUES(1,'01-01-2013','dupont');
INSERT INTO commande VALUES(2,'2014-01-05','dupond');
INSERT INTO commande VALUES(3,'2014-01-18','dupont');
INSERT INTO commande VALUES(4,'2014-01-25','dumas');
INSERT INTO commande VALUES(5,'2015-01-31','dumas');

-- format date correctly
UPDATE commande set date_com = '2013-01-01' where no_com=1;

INSERT INTO ligne_com VALUES(1,1,1);
INSERT INTO ligne_com VALUES(1,4,2);
INSERT INTO ligne_com VALUES(1,5,5);
INSERT INTO ligne_com VALUES(2,1,3);
INSERT INTO ligne_com VALUES(2,2,3);
INSERT INTO ligne_com VALUES(2,8,4);
INSERT INTO ligne_com VALUES(3,2,2);
INSERT INTO ligne_com VALUES(3,5,1);
INSERT INTO ligne_com VALUES(3,6,1);
INSERT INTO ligne_com VALUES(3,7,1);
INSERT INTO ligne_com VALUES(3,8,5);
INSERT INTO ligne_com VALUES(4,8,10);
INSERT INTO ligne_com VALUES(4,9,4);
INSERT INTO ligne_com VALUES(5,1,2);
INSERT INTO ligne_com VALUES(5,9,3);

COMMIT;

-- SELECT THE 2013 COMMANDE

select * from commande where date_com like '2013%'