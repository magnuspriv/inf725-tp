-- Exploration du schéma de la base de donnée

-- Execution de la requête select * from sqlite_master permet d'afficher les requetes de création des tables ainsi que les index
-- sqlite_sequence est une table interne utilisée pour la fonctionnalite autoincrement d'une clé primaire de type integer. 

table|sqlite_sequence|sqlite_sequence|6|CREATE TABLE sqlite_sequence(name,seq)
table|employe|employe|2|"CREATE TABLE `employe` (
	'nom_employe' text not null primary key unique,
	'salaire'	numeric,
    'no_rayon'	integer, --FK
    foreign key (no_rayon) references rayon(no_rayon)
)"
index|sqlite_autoindex_employe_1|employe|4|
table|rayon|rayon|5|"CREATE TABLE 'rayon' (
    'nom_rayon' text not null,
    'no_rayon' integer not null primary key autoincrement unique,
    'chef_rayon' text, --FK
    foreign key (chef_rayon) references employe(nom_employe)
)"
index|sqlite_autoindex_rayon_1|rayon|7|
table|produit|produit|8|"CREATE TABLE 'produit' (
    'nom_prod' text not null,
    'ref_prod' integer not null primary key autoincrement unique,
    'no_rayon' integer, --FK
    'prix_vente' numeric,
    foreign key (no_rayon) references rayon(no_rayon)
)"
index|sqlite_autoindex_produit_1|produit|9|
table|fournisseur|fournisseur|10|"CREATE TABLE 'fournisseur' (
    'nom_four' text not null primary key unique,
    'adresse_four' text
)"
index|sqlite_autoindex_fournisseur_1|fournisseur|11|
table|fournir|fournir|12|"CREATE TABLE 'fournir' (
    'nom_four' text not null, --FK
    'ref_prod' integer not null primary key autoincrement unique, --FK
    'prix' numeric not null,
    foreign key (nom_four) references fournisseur(nom_four),
    foreign key (ref_prod) references produit(ref_prod)
)"
index|sqlite_autoindex_fournir_1|fournir|13|
table|client|client|14|"CREATE TABLE 'client' (
    'nom_client' text not null primary key unique,
    'adresse_client' text,
    'solde' numeric
)"
index|sqlite_autoindex_client_1|client|15|
table|commande|commande|16|"CREATE TABLE 'commande' (
    'no_com' integer not null primary key autoincrement unique,
    'date_com' text not null,
    'nom_client' text not null, --FK
    foreign key (nom_client) references client(nom_client)
)"
index|sqlite_autoindex_commande_1|commande|17|
table|ligne_com|ligne_com|3|"CREATE TABLE 'ligne_com' (
    'no_com' integer not null, --FK
    'ref_prod' integer not null, --FK
    'quantite' numeric not null,
    foreign key (no_com) references commande(no_com),
    foreign key (ref_prod) references produit(ref_prod)
)"

-- .help de la shell sqlite affiche l'ensemble des commandes disponibles ainsi que leur description
-- .schema affiche le schema sous forme de requetes de creation de tables

sqlite> .schema
CREATE TABLE sqlite_sequence(name,seq);
CREATE TABLE `employe` (
        'nom_employe' text not null primary key unique,
        'salaire'       numeric,
    'no_rayon'  integer, --FK
    foreign key (no_rayon) references rayon(no_rayon)
);
CREATE TABLE IF NOT EXISTS 'rayon' (
    'nom_rayon' text not null,
    'no_rayon' integer not null primary key autoincrement unique,
    'chef_rayon' text, --FK
    foreign key (chef_rayon) references employe(nom_employe)
);
CREATE TABLE IF NOT EXISTS 'produit' (
    'nom_prod' text not null,
    'ref_prod' integer not null primary key autoincrement unique,
    'no_rayon' integer, --FK
    'prix_vente' numeric,
    foreign key (no_rayon) references rayon(no_rayon)
);
CREATE TABLE IF NOT EXISTS 'fournisseur' (
    'nom_four' text not null primary key unique,
    'adresse_four' text
);
CREATE TABLE IF NOT EXISTS 'fournir' (
    'nom_four' text not null, --FK
    'ref_prod' integer not null primary key autoincrement unique, --FK
    'prix' numeric not null,
    foreign key (nom_four) references fournisseur(nom_four),
    foreign key (ref_prod) references produit(ref_prod)
);
CREATE TABLE IF NOT EXISTS 'client' (
    'nom_client' text not null primary key unique,
    'adresse_client' text,
    'solde' numeric
);
CREATE TABLE IF NOT EXISTS 'commande' (
    'no_com' integer not null primary key autoincrement unique,
    'date_com' text not null,
    'nom_client' text not null, --FK
    foreign key (nom_client) references client(nom_client)
);
CREATE TABLE IF NOT EXISTS 'ligne_com' (
    'no_com' integer not null, --FK
    'ref_prod' integer not null, --FK
    'quantite' numeric not null,
    foreign key (no_com) references commande(no_com),
    foreign key (ref_prod) references produit(ref_prod)
);

-- .indexes affiche les index pour créés pour chaque table:

sqlite> .indexes
sqlite_autoindex_client_1       sqlite_autoindex_fournisseur_1
sqlite_autoindex_commande_1     sqlite_autoindex_produit_1
sqlite_autoindex_employe_1      sqlite_autoindex_rayon_1
sqlite_autoindex_fournir_1

-- .tables affiche l'ensembe des tables de la base de données:

sqlite> .tables
client       employe      fournisseur  produit
commande     fournir      ligne_com    rayon