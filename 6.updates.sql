-- Augmenter de 1000 le solde du client ‘dumas’.

update client
set solde = solde + 1000
where nom_client='dumas';

-- Supprimer les clients n'ayant jamais passé de commande.

delete from client
where nom_client in (select nom_client from (select A.nom_client, count(B.no_com) as count from client AS A
left join commande as B on A.nom_client=B.nom_client
group by A.nom_client
having count=0));

-- Supprimer toutes les informations liées au client ‘dupont’.

delete from ligne_com
where no_com in (select distinct(no_com) from (select * from client as A
left join commande as B on A.nom_client = B.nom_client
left join ligne_com as C on C.no_com = B.no_com
where A.nom_client = 'dupont'));

delete from commande
where no_com in (select distinct(no_com) from (select * from client as A
left join commande as B on A.nom_client = B.nom_client
left join ligne_com as C on C.no_com = B.no_com
where A.nom_client = 'dupont'));

delete from client
where nom_client='dupont';