PRAGMA foreign_keys=OFF;
drop table if exists 'employe';
drop table if exists 'rayon';
drop table if exists 'produit';
drop table if exists 'fournisseur';
drop table if exists 'fournir';
drop table if exists 'client';
drop table if exists 'commande';
drop table if exists 'ligne_com';

drop view if exists 'CLIENTELE_DEBITRICE';
drop view if exists 'EMPLOYE_CHEF_RAYON';
drop view if exists 'MONTANT_COMMANDE';

drop trigger if exists 'supprimer_lignes_commande';
drop trigger if exists 'verifier_prix_positif';
PRAGMA foreign_keys=ON;