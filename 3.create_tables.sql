-- SQL Script for creating schema tables

drop table if exists 'employe';
create table `employe` (
	'nom_employe' text not null primary key unique,
	'salaire'	numeric, 
    'no_rayon'	integer, --FK
    foreign key (no_rayon) references rayon(no_rayon)
);

drop table if exists 'rayon';
create table 'rayon' (
    'nom_rayon' text not null,
    'no_rayon' integer not null primary key unique,
    'chef_rayon' text, --FK
    foreign key (chef_rayon) references employe(nom_employe)
);

drop table if exists 'produit';
create table 'produit' (
    'nom_prod' text not null,
    'ref_prod' integer not null primary key unique,
    'no_rayon' integer, --FK
    'prix_vente' numeric,
    foreign key (no_rayon) references rayon(no_rayon)
);

drop table if exists 'fournisseur';
create table 'fournisseur' (
    'nom_four' text not null primary key unique,
    'adresse_four' text
);

drop table if exists 'fournir';
create table 'fournir' (
    'nom_four' text not null, --FK
    'ref_prod' integer not null, --FK
    'prix' numeric not null,
    foreign key (nom_four) references fournisseur(nom_four),
    foreign key (ref_prod) references produit(ref_prod)
);

drop table if exists 'client';
create table 'client' (
    'nom_client' text not null primary key unique,
    'adresse_client' text,
    'solde' numeric
);

drop table if exists 'commande';
create table 'commande' (
    'no_com' integer not null primary key autoincrement unique,
    'date_com' text not null,
    'nom_client' text not null, --FK
    foreign key (nom_client) references client(nom_client)
);

drop table if exists 'ligne_com';
create table 'ligne_com' (
    'no_com' integer not null, --FK
    'ref_prod' integer not null, --FK
    'quantite' numeric not null,
    foreign key (no_com) references commande(no_com),
    foreign key (ref_prod) references produit(ref_prod)
);