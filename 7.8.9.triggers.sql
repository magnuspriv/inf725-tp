-- Les triggers sont des objets associés à une table permettant l'execution d'instructions prédéfinies.
-- Les instructions du triggers sont déclenchées lorsque la table en question est affectée par un instruction de type UPDATE / DELETE / INSERT 

-- trigger permettant de supprimer automatiquement toutes les lignes d’une commande
CREATE TRIGGER IF NOT EXISTS supprimer_lignes_commande 
   BEFORE DELETE 
   ON commande 
BEGIN
  DELETE FROM ligne_com where no_com = OLD.no_com;
END

-- Testé avec la commandé numero 5 -- Ok

-- Empêcher l’insertion d’un produit avec un prix négatif.

CREATE TRIGGER verifier_prix_positif
   BEFORE INSERT 
   ON produit
BEGIN
  SELECT CASE
  WHEN NEW.prix_vente < 0
  THEN RAISE (FAIL, 'values for prix must be positive')
  END;
END;
 