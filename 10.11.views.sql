-- Créer les vues suivantes sur le schéma des questions précédentes
-- Créer les vues suivantes sur le schéma des questions précédentes
CREATE  VIEW IF NOT EXISTS CLIENTELE_DEBITRICE
AS
   select nom_client from client where solde < 0 ;

CREATE  VIEW IF NOT EXISTS EMPLOYE_CHEF_RAYON
AS
   select r.chef_rayon, e.salaire, r.nom_rayon from rayon as r
left join employe e on r.chef_rayon = e.nom_employe;


CREATE  VIEW IF NOT EXISTS MONTANT_COMMANDE
AS
   select c.no_com, c.date_com, cl.nom_client, sum(lc.quantite*p.prix_vente) as total_commande from commande as c
left join ligne_com lc on c.no_com = lc.no_com
left join client cl on c.nom_client = cl.nom_client
left join produit p on lc.ref_prod = p.ref_prod
group by c.no_com, c.date_com, cl.nom_client;

-- pour retrouver la definition des vues

select sql from sqlite_master
where type='view';

"CREATE VIEW MONTANT_COMMANDE
AS
   select c.no_com, c.date_com, cl.nom_client, sum(lc.quantite*p.prix_vente) as total_commande from commande as c
left join ligne_com lc on c.no_com = lc.no_com
left join client cl on c.nom_client = cl.nom_client
left join produit p on lc.ref_prod = p.ref_prod
group by c.no_com, c.date_com, cl.nom_client"
"CREATE VIEW CLIENTELE_DEBITRICE
AS
   select nom_client from client where solde < 0"
"CREATE VIEW EMPLOYE_CHEF_RAYON
AS
   select r.chef_rayon, e.salaire, r.nom_rayon from rayon as r
left join employe e on r.chef_rayon = e.nom_employe"
